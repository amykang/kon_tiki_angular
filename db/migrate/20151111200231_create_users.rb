class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :title
      t.text :positions
      t.integer :experience

      t.timestamps null: false
    end
  end
end
