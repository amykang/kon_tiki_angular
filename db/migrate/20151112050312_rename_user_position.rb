class RenameUserPosition < ActiveRecord::Migration
  def change
    remove_column :users, :positions
    add_column :users, :position, :string
  end
end
