class Api::UsersController < ApplicationController

  def index
    @recruiters = User.where(title: "Recruiter")
    @managers = User.where(title: "Hiring Manager")
    @seekers = User.where(title: "Job Seeker")
    @current = User.last
    render json: {"recruiters" => @recruiters, "managers" => @managers, "seekers" => @seekers, "current_user" => @current }
  end
end
