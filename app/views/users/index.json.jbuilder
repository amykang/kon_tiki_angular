json.array!(@users) do |user|
  json.extract! user, :id, :username, :title, :positions, :experience
  json.url user_url(user, format: :json)
end
