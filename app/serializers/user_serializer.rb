class UserSerializer < ActiveModel::Serializer
  attributes :id, :username, :title, :positions, :experience
end
